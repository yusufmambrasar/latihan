BACA SAYA
=========

Author
------

* Yusuf N. Mambrasar (yusuf_mambrasar@yahoo.com)
* Ahmad ahmadfitri7@gmail.com 

Capaian Kompetensi
------------------

* Dapat menginstal XAMMP pada Windows
* Dapat menginstal dan mengkonfigurasi codeigniter
* Dapat menginstal dan mengkonfigurasi AdminLTE
* Dapat menginstal dan mengkonfigurasi HMVC
* Dapat menginstal dan mengkonfigurasi ion-auth
* Dapat menginstal dan mengkonfigurasi GroceryCrud

Materi
------

#Instalasi XAMPP untuk Windows / Linux / Mac

XAMPP adalah perangkat lunak bebas, yang mendukung banyak sistem operasi, merupakan kompilasi dari beberapa program. Fungsinya adalah sebagai server yang berdiri sendiri, yang terdiri atas program Apache HTTP Server, MySQL database, dan penerjemah bahasa yang ditulis dengan bahasa pemrograman PHP dan Perl. Sumber: https://id.wikipedia.org/wiki/XAMPP 

##XAMPP Portable

* Download XAMPP Portable di https//sourceforge.net/projects/xampp/ 
* Pilih yang di kompresi zip/7z
* Ektrak filenya
* Jalankan setup_xampp.bat
* Jalankan XAMPP Control Panel
* Jalankan Apache2 dan MySQL 
* Jalankan browser ke http://localhost 

##XAMPP Non Portable

* Download XAMPP Portable di https//sourceforge.net/projects/xampp/ 
* Pilih yang di berkestensi .exe
* Jalankan file berkestensi .exe tersebut
* Jalankan XAMPP Control Panel
* Jalankan Apache2 dan MySQL 
* Jalankan browser ke http://localhost 

#Instalasi dan Konfigurasi Codeigniter

##Instalasi

* Download codeigniter di https://www.codeigniter.com/download
* Ektrak filenya
* Buat folder di folder htdocs misalnya latihan
* Kopikan seluruh hasilekstrakan codeigniter ke dalam folder latihan 
* Jalankan Apache2 dan MySQL 
* Jalankan browser ke http://localhost/latihan/ 

##Konfigurasi 

###User Friendly URL

* Buka file config pada folder ./latihan/application/config/config.php 
* Ganti krip base_url berikut:
```
$config['base_url'] = '';
```
Menjadi:
```
$config['base_url'] = 'http://localhost/latihan/';
```
* Ganti skrip index_page berikut:
```
$config['index_page'] = 'index.php';
```
Menjadi:
```
$config['index_page'] = '';
```
* Buat file ./.htaccess
* Isi skrip berikut:
```
RewriteEngine on
RewriteBase /latihan/
RewriteCond $1 !^(index\.php|import\.php|uploads|assets|robots\.txt)
RewriteRule ^(.*)$ /latihan/index.php/$1 [L]

php_value upload_max_filesize 1000M
php_value post_max_size 1000M
```

#Instalasi dan Konfigurasi AdminLTE

* Download AdminLTE2 dari https://adminlte.io/download/AdminLTE-master
* Ekstrak file yang sudah di download
* Buat folder ./assets/
* Buat folder ./assets/adminlte2/
* Kopikan semua isi file AdminLTE yang sudah di ektrak ke folder ./assets/adminlte2/
* Kopikan file ./assets/adminlte2/index.html menjadi ./application/views/welcome.php
* Tambahkan skrip dibawah ini ke bagian atas ./application/views/welcome.php
'''
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
'''
* Pada file ./application/views/welcome.php cari setiap skrip yang berisi
'''
href="{isi_href}"
'''
dan 
'''
src="{isi_src}"
'''
menjadi:
'''
href="<?= site_url('{isi_href}'); ?>"
'''
dan
'''
src="<?= site_url('{isi_href}'); ?>"
'''
* Buka file ./application/controllers/Welcome.php 
* Ganti skrip berikut:
'''
$this->load->view('welcome_message');
'''
menjadi:
'''
$this->load->view('welcome');
'''
* Buka file ./application/config/autolad.php 
* Cari baris berikut:
'''
$autoload['helper'] = array();
'''
* Ganti menjadi:
'''
$autoload['helper'] = array('url');
'''
* Simpan hasil kerjaan Anda 
* Buka alamat http://localhost/latihan/di browser kesayangan Anda

Langkah-langkah diatas fungsinya:
Membuat view untuk halaman utama dengan tampilan AdminLTE

#Instalasi dan Konfigurasi HMVC
#Instalasi dan Konfigurasi ion-auth 
#Instalasi dan Konfigurasi GroceryCrud

Aplikasi
---------
* Visual Studio Code
* Git
* TortoiseGit

Plugin untuk Visual Studio Code
-------------------------------

Name: Git History
Id: donjayamanne.githistory
Description: View git log, file history, compare branches or commits
Version: 0.4.6
Publisher: Don Jayamanne
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=donjayamanne.githistory

Name: Git Project Manager
Id: felipecaputo.git-project-manager
Description: Allows you to change easily between git projects.
Version: 1.7.1
Publisher: Felipe Caputo
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=felipecaputo.git-project-manager

Name: gitignore
Id: michelemelluso.gitignore
Description: Add file to .gitignore
Version: 1.0.1
Publisher: michelemelluso
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=michelemelluso.gitignore

Name: PHP Debug
Id: felixfbecker.php-debug
Description: Debug support for PHP with XDebug
Version: 1.13.0
Publisher: Felix Becker
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-debug

Name: PHP Extension Pack
Id: felixfbecker.php-pack
Description: Everything you need for PHP development
Version: 1.0.2
Publisher: Felix Becker
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-pack

Name: PHP IntelliSense
Id: felixfbecker.php-intellisense
Description: Advanced Autocompletion and Refactoring support for PHP
Version: 2.3.10
Publisher: Felix Becker
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-intellisense

Name: phpdoc-comment-vscode-plugin
Id: rexshi.phpdoc-comment-vscode-plugin
Description: Add phpdoc @param and @return tag for selected function signatures.
Version: 1.1.0
Publisher: Rex Shi
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=rexshi.phpdoc-comment-vscode-plugin

Name: Project Manager
Id: alefragnani.project-manager
Description: Easily switch between projects
Version: 10.5.2
Publisher: Alessandro Fragnani
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=alefragnani.project-manager

Name: Todo+
Id: fabiospampinato.vscode-todo-plus
Description: Manage todo lists with ease. Powerful, easy to use and customizable.
Version: 4.13.0
Publisher: Fabio Spampinato
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=fabiospampinato.vscode-todo-plus

Name: TODO Highlight
Id: wayou.vscode-todo-highlight
Description: highlight TODOs, FIXMEs, and any keywords, annotations...
Version: 1.0.4
Publisher: Wayou Liu
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=wayou.vscode-todo-highlight

Sumber Belajar 
--------------
* https://bitbucket.org/yusufmambrasar/latihan/src/master/
* https://github.com/yusufmambrasar/latihan