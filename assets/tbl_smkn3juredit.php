<?php
namespace PHPMaker2019\PPDBSMK2019;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$tbl_smkn3jur_edit = new tbl_smkn3jur_edit();

// Run the page
$tbl_smkn3jur_edit->run();

// Setup login status
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$tbl_smkn3jur_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "edit";
var ftbl_smkn3juredit = currentForm = new ew.Form("ftbl_smkn3juredit", "edit");

// Validate form
ftbl_smkn3juredit.validate = function() {
	if (!this.validateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.getForm(), $fobj = $(fobj);
	if ($fobj.find("#confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.formKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = ["insert", "gridinsert"].includes($fobj.find("#action").val()) && $k[0];
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		<?php if ($tbl_smkn3jur_edit->id_smkn2jur->Required) { ?>
			elm = this.getElements("x" + infix + "_id_smkn2jur");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $tbl_smkn3jur->id_smkn2jur->caption(), $tbl_smkn3jur->id_smkn2jur->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($tbl_smkn3jur_edit->Jurusan->Required) { ?>
			elm = this.getElements("x" + infix + "_Jurusan");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $tbl_smkn3jur->Jurusan->caption(), $tbl_smkn3jur->Jurusan->RequiredErrorMessage)) ?>");
		<?php } ?>

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ew.forms[val])
			if (!ew.forms[val].validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
ftbl_smkn3juredit.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
ftbl_smkn3juredit.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
// Form object for search

</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php $tbl_smkn3jur_edit->showPageHeader(); ?>
<?php
$tbl_smkn3jur_edit->showMessage();
?>
<form name="ftbl_smkn3juredit" id="ftbl_smkn3juredit" class="<?php echo $tbl_smkn3jur_edit->FormClassName ?>" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($tbl_smkn3jur_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $tbl_smkn3jur_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="tbl_smkn3jur">
<input type="hidden" name="action" id="action" value="update">
<input type="hidden" name="modal" value="<?php echo (int)$tbl_smkn3jur_edit->IsModal ?>">
<div class="ew-edit-div"><!-- page* -->
<?php if ($tbl_smkn3jur->id_smkn2jur->Visible) { // id_smkn2jur ?>
	<div id="r_id_smkn2jur" class="form-group row">
		<label id="elh_tbl_smkn3jur_id_smkn2jur" class="<?php echo $tbl_smkn3jur_edit->LeftColumnClass ?>"><?php echo $tbl_smkn3jur->id_smkn2jur->caption() ?><?php echo ($tbl_smkn3jur->id_smkn2jur->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $tbl_smkn3jur_edit->RightColumnClass ?>"><div<?php echo $tbl_smkn3jur->id_smkn2jur->cellAttributes() ?>>
<span id="el_tbl_smkn3jur_id_smkn2jur">
<span<?php echo $tbl_smkn3jur->id_smkn2jur->viewAttributes() ?>>
<input type="text" readonly class="form-control-plaintext" value="<?php echo RemoveHtml($tbl_smkn3jur->id_smkn2jur->EditValue) ?>"></span>
</span>
<input type="hidden" data-table="tbl_smkn3jur" data-field="x_id_smkn2jur" name="x_id_smkn2jur" id="x_id_smkn2jur" value="<?php echo HtmlEncode($tbl_smkn3jur->id_smkn2jur->CurrentValue) ?>">
<?php echo $tbl_smkn3jur->id_smkn2jur->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($tbl_smkn3jur->Jurusan->Visible) { // Jurusan ?>
	<div id="r_Jurusan" class="form-group row">
		<label id="elh_tbl_smkn3jur_Jurusan" for="x_Jurusan" class="<?php echo $tbl_smkn3jur_edit->LeftColumnClass ?>"><?php echo $tbl_smkn3jur->Jurusan->caption() ?><?php echo ($tbl_smkn3jur->Jurusan->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $tbl_smkn3jur_edit->RightColumnClass ?>"><div<?php echo $tbl_smkn3jur->Jurusan->cellAttributes() ?>>
<span id="el_tbl_smkn3jur_Jurusan">
<input type="text" data-table="tbl_smkn3jur" data-field="x_Jurusan" name="x_Jurusan" id="x_Jurusan" size="30" maxlength="50" placeholder="<?php echo HtmlEncode($tbl_smkn3jur->Jurusan->getPlaceHolder()) ?>" value="<?php echo $tbl_smkn3jur->Jurusan->EditValue ?>"<?php echo $tbl_smkn3jur->Jurusan->editAttributes() ?>>
</span>
<?php echo $tbl_smkn3jur->Jurusan->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div><!-- /page* -->
<?php if (!$tbl_smkn3jur_edit->IsModal) { ?>
<div class="form-group row"><!-- buttons .form-group -->
	<div class="<?php echo $tbl_smkn3jur_edit->OffsetColumnClass ?>"><!-- buttons offset -->
<button class="btn btn-primary ew-btn" name="btn-action" id="btn-action" type="submit"><?php echo $Language->phrase("SaveBtn") ?></button>
<button class="btn btn-default ew-btn" name="btn-cancel" id="btn-cancel" type="button" data-href="<?php echo $tbl_smkn3jur_edit->getReturnUrl() ?>"><?php echo $Language->phrase("CancelBtn") ?></button>
	</div><!-- /buttons offset -->
</div><!-- /buttons .form-group -->
<?php } ?>
</form>
<?php
$tbl_smkn3jur_edit->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$tbl_smkn3jur_edit->terminate();
?>