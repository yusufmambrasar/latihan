<?php
namespace PHPMaker2019\PPDBSMK2019;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$tbl_daftar_list = new tbl_daftar_list();

// Run the page
$tbl_daftar_list->run();

// Setup login status
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$tbl_daftar_list->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if (!$tbl_daftar->isExport()) { ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "list";
var ftbl_daftarlist = currentForm = new ew.Form("ftbl_daftarlist", "list");
ftbl_daftarlist.formKeyCountName = '<?php echo $tbl_daftar_list->FormKeyCountName ?>';

// Form_CustomValidate event
ftbl_daftarlist.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
ftbl_daftarlist.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
ftbl_daftarlist.lists["x_Sekolah"] = <?php echo $tbl_daftar_list->Sekolah->Lookup->toClientList() ?>;
ftbl_daftarlist.lists["x_Sekolah"].options = <?php echo JsonEncode($tbl_daftar_list->Sekolah->lookupOptions()) ?>;
ftbl_daftarlist.lists["x_Jurusan"] = <?php echo $tbl_daftar_list->Jurusan->Lookup->toClientList() ?>;
ftbl_daftarlist.lists["x_Jurusan"].options = <?php echo JsonEncode($tbl_daftar_list->Jurusan->lookupOptions()) ?>;

// Form object for search
var ftbl_daftarlistsrch = currentSearchForm = new ew.Form("ftbl_daftarlistsrch");

// Filters
ftbl_daftarlistsrch.filterList = <?php echo $tbl_daftar_list->getFilterList() ?>;
</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if (!$tbl_daftar->isExport()) { ?>
<div class="btn-toolbar ew-toolbar">
<?php if ($tbl_daftar_list->TotalRecs > 0 && $tbl_daftar_list->ExportOptions->visible()) { ?>
<?php $tbl_daftar_list->ExportOptions->render("body") ?>
<?php } ?>
<?php if ($tbl_daftar_list->ImportOptions->visible()) { ?>
<?php $tbl_daftar_list->ImportOptions->render("body") ?>
<?php } ?>
<?php if ($tbl_daftar_list->SearchOptions->visible()) { ?>
<?php $tbl_daftar_list->SearchOptions->render("body") ?>
<?php } ?>
<?php if ($tbl_daftar_list->FilterOptions->visible()) { ?>
<?php $tbl_daftar_list->FilterOptions->render("body") ?>
<?php } ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php
$tbl_daftar_list->renderOtherOptions();
?>
<?php if (!$tbl_daftar->isExport() && !$tbl_daftar->CurrentAction) { ?>
<form name="ftbl_daftarlistsrch" id="ftbl_daftarlistsrch" class="form-inline ew-form ew-ext-search-form" action="<?php echo CurrentPageName() ?>">
<?php $searchPanelClass = ($tbl_daftar_list->SearchWhere <> "") ? " show" : " show"; ?>
<div id="ftbl_daftarlistsrch-search-panel" class="ew-search-panel collapse<?php echo $searchPanelClass ?>">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="tbl_daftar">
	<div class="ew-basic-search">
<div id="xsr_1" class="ew-row d-sm-flex">
	<div class="ew-quick-search input-group">
		<input type="text" name="<?php echo TABLE_BASIC_SEARCH ?>" id="<?php echo TABLE_BASIC_SEARCH ?>" class="form-control" value="<?php echo HtmlEncode($tbl_daftar_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo HtmlEncode($Language->phrase("Search")) ?>">
		<input type="hidden" name="<?php echo TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo TABLE_BASIC_SEARCH_TYPE ?>" value="<?php echo HtmlEncode($tbl_daftar_list->BasicSearch->getType()) ?>">
		<div class="input-group-append">
			<button class="btn btn-primary" name="btn-submit" id="btn-submit" type="submit"><?php echo $Language->phrase("SearchBtn") ?></button>
			<button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle dropdown-toggle-split" aria-haspopup="true" aria-expanded="false"><span id="searchtype"><?php echo $tbl_daftar_list->BasicSearch->getTypeNameShort() ?></span></button>
			<div class="dropdown-menu dropdown-menu-right">
				<a class="dropdown-item<?php if ($tbl_daftar_list->BasicSearch->getType() == "") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this)"><?php echo $Language->phrase("QuickSearchAuto") ?></a>
				<a class="dropdown-item<?php if ($tbl_daftar_list->BasicSearch->getType() == "=") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this,'=')"><?php echo $Language->phrase("QuickSearchExact") ?></a>
				<a class="dropdown-item<?php if ($tbl_daftar_list->BasicSearch->getType() == "AND") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this,'AND')"><?php echo $Language->phrase("QuickSearchAll") ?></a>
				<a class="dropdown-item<?php if ($tbl_daftar_list->BasicSearch->getType() == "OR") echo " active"; ?>" href="javascript:void(0);" onclick="ew.setSearchType(this,'OR')"><?php echo $Language->phrase("QuickSearchAny") ?></a>
			</div>
		</div>
	</div>
</div>
	</div>
</div>
</form>
<?php } ?>
<?php $tbl_daftar_list->showPageHeader(); ?>
<?php
$tbl_daftar_list->showMessage();
?>
<?php if ($tbl_daftar_list->TotalRecs > 0 || $tbl_daftar->CurrentAction) { ?>
<div class="card ew-card ew-grid<?php if ($tbl_daftar_list->isAddOrEdit()) { ?> ew-grid-add-edit<?php } ?> tbl_daftar">
<form name="ftbl_daftarlist" id="ftbl_daftarlist" class="form-inline ew-form ew-list-form" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($tbl_daftar_list->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $tbl_daftar_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="tbl_daftar">
<div id="gmp_tbl_daftar" class="<?php if (IsResponsiveLayout()) { ?>table-responsive <?php } ?>card-body ew-grid-middle-panel">
<?php if ($tbl_daftar_list->TotalRecs > 0 || $tbl_daftar->isGridEdit()) { ?>
<table id="tbl_tbl_daftarlist" class="table ew-table"><!-- .ew-table ##-->
<thead>
	<tr class="ew-table-header">
<?php

// Header row
$tbl_daftar_list->RowType = ROWTYPE_HEADER;

// Render list options
$tbl_daftar_list->renderListOptions();

// Render list options (header, left)
$tbl_daftar_list->ListOptions->render("header", "left");
?>
<?php if ($tbl_daftar->Sekolah->Visible) { // Sekolah ?>
	<?php if ($tbl_daftar->sortUrl($tbl_daftar->Sekolah) == "") { ?>
		<th data-name="Sekolah" class="<?php echo $tbl_daftar->Sekolah->headerCellClass() ?>"><div id="elh_tbl_daftar_Sekolah" class="tbl_daftar_Sekolah"><div class="ew-table-header-caption"><?php echo $tbl_daftar->Sekolah->caption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="Sekolah" class="<?php echo $tbl_daftar->Sekolah->headerCellClass() ?>"><div class="ew-pointer" onclick="ew.sort(event,'<?php echo $tbl_daftar->SortUrl($tbl_daftar->Sekolah) ?>',1);"><div id="elh_tbl_daftar_Sekolah" class="tbl_daftar_Sekolah">
			<div class="ew-table-header-btn"><span class="ew-table-header-caption"><?php echo $tbl_daftar->Sekolah->caption() ?></span><span class="ew-table-header-sort"><?php if ($tbl_daftar->Sekolah->getSort() == "ASC") { ?><i class="fa fa-sort-up"></i><?php } elseif ($tbl_daftar->Sekolah->getSort() == "DESC") { ?><i class="fa fa-sort-down"></i><?php } ?></span></div>
		</div></div></th>
	<?php } ?>
<?php } ?>
<?php if ($tbl_daftar->Jurusan->Visible) { // Jurusan ?>
	<?php if ($tbl_daftar->sortUrl($tbl_daftar->Jurusan) == "") { ?>
		<th data-name="Jurusan" class="<?php echo $tbl_daftar->Jurusan->headerCellClass() ?>"><div id="elh_tbl_daftar_Jurusan" class="tbl_daftar_Jurusan"><div class="ew-table-header-caption"><?php echo $tbl_daftar->Jurusan->caption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="Jurusan" class="<?php echo $tbl_daftar->Jurusan->headerCellClass() ?>"><div class="ew-pointer" onclick="ew.sort(event,'<?php echo $tbl_daftar->SortUrl($tbl_daftar->Jurusan) ?>',1);"><div id="elh_tbl_daftar_Jurusan" class="tbl_daftar_Jurusan">
			<div class="ew-table-header-btn"><span class="ew-table-header-caption"><?php echo $tbl_daftar->Jurusan->caption() ?></span><span class="ew-table-header-sort"><?php if ($tbl_daftar->Jurusan->getSort() == "ASC") { ?><i class="fa fa-sort-up"></i><?php } elseif ($tbl_daftar->Jurusan->getSort() == "DESC") { ?><i class="fa fa-sort-down"></i><?php } ?></span></div>
		</div></div></th>
	<?php } ?>
<?php } ?>
<?php

// Render list options (header, right)
$tbl_daftar_list->ListOptions->render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($tbl_daftar->ExportAll && $tbl_daftar->isExport()) {
	$tbl_daftar_list->StopRec = $tbl_daftar_list->TotalRecs;
} else {

	// Set the last record to display
	if ($tbl_daftar_list->TotalRecs > $tbl_daftar_list->StartRec + $tbl_daftar_list->DisplayRecs - 1)
		$tbl_daftar_list->StopRec = $tbl_daftar_list->StartRec + $tbl_daftar_list->DisplayRecs - 1;
	else
		$tbl_daftar_list->StopRec = $tbl_daftar_list->TotalRecs;
}
$tbl_daftar_list->RecCnt = $tbl_daftar_list->StartRec - 1;
if ($tbl_daftar_list->Recordset && !$tbl_daftar_list->Recordset->EOF) {
	$tbl_daftar_list->Recordset->moveFirst();
	$selectLimit = $tbl_daftar_list->UseSelectLimit;
	if (!$selectLimit && $tbl_daftar_list->StartRec > 1)
		$tbl_daftar_list->Recordset->move($tbl_daftar_list->StartRec - 1);
} elseif (!$tbl_daftar->AllowAddDeleteRow && $tbl_daftar_list->StopRec == 0) {
	$tbl_daftar_list->StopRec = $tbl_daftar->GridAddRowCount;
}

// Initialize aggregate
$tbl_daftar->RowType = ROWTYPE_AGGREGATEINIT;
$tbl_daftar->resetAttributes();
$tbl_daftar_list->renderRow();
while ($tbl_daftar_list->RecCnt < $tbl_daftar_list->StopRec) {
	$tbl_daftar_list->RecCnt++;
	if ($tbl_daftar_list->RecCnt >= $tbl_daftar_list->StartRec) {
		$tbl_daftar_list->RowCnt++;

		// Set up key count
		$tbl_daftar_list->KeyCount = $tbl_daftar_list->RowIndex;

		// Init row class and style
		$tbl_daftar->resetAttributes();
		$tbl_daftar->CssClass = "";
		if ($tbl_daftar->isGridAdd()) {
		} else {
			$tbl_daftar_list->loadRowValues($tbl_daftar_list->Recordset); // Load row values
		}
		$tbl_daftar->RowType = ROWTYPE_VIEW; // Render view

		// Set up row id / data-rowindex
		$tbl_daftar->RowAttrs = array_merge($tbl_daftar->RowAttrs, array('data-rowindex'=>$tbl_daftar_list->RowCnt, 'id'=>'r' . $tbl_daftar_list->RowCnt . '_tbl_daftar', 'data-rowtype'=>$tbl_daftar->RowType));

		// Render row
		$tbl_daftar_list->renderRow();

		// Render list options
		$tbl_daftar_list->renderListOptions();
?>
	<tr<?php echo $tbl_daftar->rowAttributes() ?>>
<?php

// Render list options (body, left)
$tbl_daftar_list->ListOptions->render("body", "left", $tbl_daftar_list->RowCnt);
?>
	<?php if ($tbl_daftar->Sekolah->Visible) { // Sekolah ?>
		<td data-name="Sekolah"<?php echo $tbl_daftar->Sekolah->cellAttributes() ?>>
<span id="el<?php echo $tbl_daftar_list->RowCnt ?>_tbl_daftar_Sekolah" class="tbl_daftar_Sekolah">
<span<?php echo $tbl_daftar->Sekolah->viewAttributes() ?>>
<?php echo $tbl_daftar->Sekolah->getViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($tbl_daftar->Jurusan->Visible) { // Jurusan ?>
		<td data-name="Jurusan"<?php echo $tbl_daftar->Jurusan->cellAttributes() ?>>
<span id="el<?php echo $tbl_daftar_list->RowCnt ?>_tbl_daftar_Jurusan" class="tbl_daftar_Jurusan">
<span<?php echo $tbl_daftar->Jurusan->viewAttributes() ?>>
<?php echo $tbl_daftar->Jurusan->getViewValue() ?></span>
</span>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$tbl_daftar_list->ListOptions->render("body", "right", $tbl_daftar_list->RowCnt);
?>
	</tr>
<?php
	}
	if (!$tbl_daftar->isGridAdd())
		$tbl_daftar_list->Recordset->moveNext();
}
?>
</tbody>
</table><!-- /.ew-table -->
<?php } ?>
<?php if (!$tbl_daftar->CurrentAction) { ?>
<input type="hidden" name="action" id="action" value="">
<?php } ?>
</div><!-- /.ew-grid-middle-panel -->
</form><!-- /.ew-list-form -->
<?php

// Close recordset
if ($tbl_daftar_list->Recordset)
	$tbl_daftar_list->Recordset->Close();
?>
<?php if (!$tbl_daftar->isExport()) { ?>
<div class="card-footer ew-grid-lower-panel">
<?php if (!$tbl_daftar->isGridAdd()) { ?>
<form name="ew-pager-form" class="form-inline ew-form ew-pager-form" action="<?php echo CurrentPageName() ?>">
<?php if (!isset($tbl_daftar_list->Pager)) $tbl_daftar_list->Pager = new PrevNextPager($tbl_daftar_list->StartRec, $tbl_daftar_list->DisplayRecs, $tbl_daftar_list->TotalRecs, $tbl_daftar_list->AutoHidePager) ?>
<?php if ($tbl_daftar_list->Pager->RecordCount > 0 && $tbl_daftar_list->Pager->Visible) { ?>
<div class="ew-pager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ew-prev-next"><div class="input-group input-group-sm">
<div class="input-group-prepend">
<!-- first page button -->
	<?php if ($tbl_daftar_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerFirst") ?>" href="<?php echo $tbl_daftar_list->pageUrl() ?>start=<?php echo $tbl_daftar_list->Pager->FirstButton->Start ?>"><i class="icon-first ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerFirst") ?>"><i class="icon-first ew-icon"></i></a>
	<?php } ?>
<!-- previous page button -->
	<?php if ($tbl_daftar_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerPrevious") ?>" href="<?php echo $tbl_daftar_list->pageUrl() ?>start=<?php echo $tbl_daftar_list->Pager->PrevButton->Start ?>"><i class="icon-prev ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerPrevious") ?>"><i class="icon-prev ew-icon"></i></a>
	<?php } ?>
</div>
<!-- current page number -->
	<input class="form-control" type="text" name="<?php echo TABLE_PAGE_NO ?>" value="<?php echo $tbl_daftar_list->Pager->CurrentPage ?>">
<div class="input-group-append">
<!-- next page button -->
	<?php if ($tbl_daftar_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerNext") ?>" href="<?php echo $tbl_daftar_list->pageUrl() ?>start=<?php echo $tbl_daftar_list->Pager->NextButton->Start ?>"><i class="icon-next ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerNext") ?>"><i class="icon-next ew-icon"></i></a>
	<?php } ?>
<!-- last page button -->
	<?php if ($tbl_daftar_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default" title="<?php echo $Language->phrase("PagerLast") ?>" href="<?php echo $tbl_daftar_list->pageUrl() ?>start=<?php echo $tbl_daftar_list->Pager->LastButton->Start ?>"><i class="icon-last ew-icon"></i></a>
	<?php } else { ?>
	<a class="btn btn-default disabled" title="<?php echo $Language->phrase("PagerLast") ?>"><i class="icon-last ew-icon"></i></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $tbl_daftar_list->Pager->PageCount ?></span>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php if ($tbl_daftar_list->Pager->RecordCount > 0) { ?>
<div class="ew-pager ew-rec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $tbl_daftar_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $tbl_daftar_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $tbl_daftar_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ew-list-other-options">
<?php $tbl_daftar_list->OtherOptions->render("body", "bottom") ?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
</div><!-- /.ew-grid -->
<?php } ?>
<?php if ($tbl_daftar_list->TotalRecs == 0 && !$tbl_daftar->CurrentAction) { // Show other options ?>
<div class="ew-list-other-options">
<?php $tbl_daftar_list->OtherOptions->render("body") ?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php
$tbl_daftar_list->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<?php if (!$tbl_daftar->isExport()) { ?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$tbl_daftar_list->terminate();
?>