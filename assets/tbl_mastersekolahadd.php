<?php
namespace PHPMaker2019\PPDBSMK2019;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$tbl_mastersekolah_add = new tbl_mastersekolah_add();

// Run the page
$tbl_mastersekolah_add->run();

// Setup login status
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$tbl_mastersekolah_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "add";
var ftbl_mastersekolahadd = currentForm = new ew.Form("ftbl_mastersekolahadd", "add");

// Validate form
ftbl_mastersekolahadd.validate = function() {
	if (!this.validateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.getForm(), $fobj = $(fobj);
	if ($fobj.find("#confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.formKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = ["insert", "gridinsert"].includes($fobj.find("#action").val()) && $k[0];
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		<?php if ($tbl_mastersekolah_add->Sekolah->Required) { ?>
			elm = this.getElements("x" + infix + "_Sekolah");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $tbl_mastersekolah->Sekolah->caption(), $tbl_mastersekolah->Sekolah->RequiredErrorMessage)) ?>");
		<?php } ?>

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ew.forms[val])
			if (!ew.forms[val].validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
ftbl_mastersekolahadd.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
ftbl_mastersekolahadd.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
// Form object for search

</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php $tbl_mastersekolah_add->showPageHeader(); ?>
<?php
$tbl_mastersekolah_add->showMessage();
?>
<form name="ftbl_mastersekolahadd" id="ftbl_mastersekolahadd" class="<?php echo $tbl_mastersekolah_add->FormClassName ?>" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($tbl_mastersekolah_add->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $tbl_mastersekolah_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="tbl_mastersekolah">
<input type="hidden" name="action" id="action" value="insert">
<input type="hidden" name="modal" value="<?php echo (int)$tbl_mastersekolah_add->IsModal ?>">
<div class="ew-add-div"><!-- page* -->
<?php if ($tbl_mastersekolah->Sekolah->Visible) { // Sekolah ?>
	<div id="r_Sekolah" class="form-group row">
		<label id="elh_tbl_mastersekolah_Sekolah" for="x_Sekolah" class="<?php echo $tbl_mastersekolah_add->LeftColumnClass ?>"><?php echo $tbl_mastersekolah->Sekolah->caption() ?><?php echo ($tbl_mastersekolah->Sekolah->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $tbl_mastersekolah_add->RightColumnClass ?>"><div<?php echo $tbl_mastersekolah->Sekolah->cellAttributes() ?>>
<span id="el_tbl_mastersekolah_Sekolah">
<input type="text" data-table="tbl_mastersekolah" data-field="x_Sekolah" name="x_Sekolah" id="x_Sekolah" size="30" maxlength="50" placeholder="<?php echo HtmlEncode($tbl_mastersekolah->Sekolah->getPlaceHolder()) ?>" value="<?php echo $tbl_mastersekolah->Sekolah->EditValue ?>"<?php echo $tbl_mastersekolah->Sekolah->editAttributes() ?>>
</span>
<?php echo $tbl_mastersekolah->Sekolah->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div><!-- /page* -->
<?php if (!$tbl_mastersekolah_add->IsModal) { ?>
<div class="form-group row"><!-- buttons .form-group -->
	<div class="<?php echo $tbl_mastersekolah_add->OffsetColumnClass ?>"><!-- buttons offset -->
<button class="btn btn-primary ew-btn" name="btn-action" id="btn-action" type="submit"><?php echo $Language->phrase("AddBtn") ?></button>
<button class="btn btn-default ew-btn" name="btn-cancel" id="btn-cancel" type="button" data-href="<?php echo $tbl_mastersekolah_add->getReturnUrl() ?>"><?php echo $Language->phrase("CancelBtn") ?></button>
	</div><!-- /buttons offset -->
</div><!-- /buttons .form-group -->
<?php } ?>
</form>
<?php
$tbl_mastersekolah_add->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$tbl_mastersekolah_add->terminate();
?>