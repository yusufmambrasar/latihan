<?php
namespace PHPMaker2019\PPDBSMK2019;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$tbl_smkn2_delete = new tbl_smkn2_delete();

// Run the page
$tbl_smkn2_delete->run();

// Setup login status
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$tbl_smkn2_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "delete";
var ftbl_smkn2delete = currentForm = new ew.Form("ftbl_smkn2delete", "delete");

// Form_CustomValidate event
ftbl_smkn2delete.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
ftbl_smkn2delete.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
ftbl_smkn2delete.lists["x_Sekolah"] = <?php echo $tbl_smkn2_delete->Sekolah->Lookup->toClientList() ?>;
ftbl_smkn2delete.lists["x_Sekolah"].options = <?php echo JsonEncode($tbl_smkn2_delete->Sekolah->lookupOptions()) ?>;
ftbl_smkn2delete.lists["x_id_smkn2jur"] = <?php echo $tbl_smkn2_delete->id_smkn2jur->Lookup->toClientList() ?>;
ftbl_smkn2delete.lists["x_id_smkn2jur"].options = <?php echo JsonEncode($tbl_smkn2_delete->id_smkn2jur->lookupOptions()) ?>;

// Form object for search
</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php $tbl_smkn2_delete->showPageHeader(); ?>
<?php
$tbl_smkn2_delete->showMessage();
?>
<form name="ftbl_smkn2delete" id="ftbl_smkn2delete" class="form-inline ew-form ew-delete-form" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($tbl_smkn2_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $tbl_smkn2_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="tbl_smkn2">
<input type="hidden" name="action" id="action" value="delete">
<?php foreach ($tbl_smkn2_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="card ew-card ew-grid">
<div class="<?php if (IsResponsiveLayout()) { ?>table-responsive <?php } ?>card-body ew-grid-middle-panel">
<table class="table ew-table">
	<thead>
	<tr class="ew-table-header">
<?php if ($tbl_smkn2->id_sekolah->Visible) { // id_sekolah ?>
		<th class="<?php echo $tbl_smkn2->id_sekolah->headerCellClass() ?>"><span id="elh_tbl_smkn2_id_sekolah" class="tbl_smkn2_id_sekolah"><?php echo $tbl_smkn2->id_sekolah->caption() ?></span></th>
<?php } ?>
<?php if ($tbl_smkn2->Sekolah->Visible) { // Sekolah ?>
		<th class="<?php echo $tbl_smkn2->Sekolah->headerCellClass() ?>"><span id="elh_tbl_smkn2_Sekolah" class="tbl_smkn2_Sekolah"><?php echo $tbl_smkn2->Sekolah->caption() ?></span></th>
<?php } ?>
<?php if ($tbl_smkn2->id_smkn2jur->Visible) { // id_smkn2jur ?>
		<th class="<?php echo $tbl_smkn2->id_smkn2jur->headerCellClass() ?>"><span id="elh_tbl_smkn2_id_smkn2jur" class="tbl_smkn2_id_smkn2jur"><?php echo $tbl_smkn2->id_smkn2jur->caption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$tbl_smkn2_delete->RecCnt = 0;
$i = 0;
while (!$tbl_smkn2_delete->Recordset->EOF) {
	$tbl_smkn2_delete->RecCnt++;
	$tbl_smkn2_delete->RowCnt++;

	// Set row properties
	$tbl_smkn2->resetAttributes();
	$tbl_smkn2->RowType = ROWTYPE_VIEW; // View

	// Get the field contents
	$tbl_smkn2_delete->loadRowValues($tbl_smkn2_delete->Recordset);

	// Render row
	$tbl_smkn2_delete->renderRow();
?>
	<tr<?php echo $tbl_smkn2->rowAttributes() ?>>
<?php if ($tbl_smkn2->id_sekolah->Visible) { // id_sekolah ?>
		<td<?php echo $tbl_smkn2->id_sekolah->cellAttributes() ?>>
<span id="el<?php echo $tbl_smkn2_delete->RowCnt ?>_tbl_smkn2_id_sekolah" class="tbl_smkn2_id_sekolah">
<span<?php echo $tbl_smkn2->id_sekolah->viewAttributes() ?>>
<?php echo $tbl_smkn2->id_sekolah->getViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($tbl_smkn2->Sekolah->Visible) { // Sekolah ?>
		<td<?php echo $tbl_smkn2->Sekolah->cellAttributes() ?>>
<span id="el<?php echo $tbl_smkn2_delete->RowCnt ?>_tbl_smkn2_Sekolah" class="tbl_smkn2_Sekolah">
<span<?php echo $tbl_smkn2->Sekolah->viewAttributes() ?>>
<?php echo $tbl_smkn2->Sekolah->getViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($tbl_smkn2->id_smkn2jur->Visible) { // id_smkn2jur ?>
		<td<?php echo $tbl_smkn2->id_smkn2jur->cellAttributes() ?>>
<span id="el<?php echo $tbl_smkn2_delete->RowCnt ?>_tbl_smkn2_id_smkn2jur" class="tbl_smkn2_id_smkn2jur">
<span<?php echo $tbl_smkn2->id_smkn2jur->viewAttributes() ?>>
<?php echo $tbl_smkn2->id_smkn2jur->getViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$tbl_smkn2_delete->Recordset->moveNext();
}
$tbl_smkn2_delete->Recordset->close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ew-btn" name="btn-action" id="btn-action" type="submit"><?php echo $Language->phrase("DeleteBtn") ?></button>
<button class="btn btn-default ew-btn" name="btn-cancel" id="btn-cancel" type="button" data-href="<?php echo $tbl_smkn2_delete->getReturnUrl() ?>"><?php echo $Language->phrase("CancelBtn") ?></button>
</div>
</form>
<?php
$tbl_smkn2_delete->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$tbl_smkn2_delete->terminate();
?>