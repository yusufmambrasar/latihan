<?php
namespace PHPMaker2019\PPDBSMK2019;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$tbl_masterjursan_view = new tbl_masterjursan_view();

// Run the page
$tbl_masterjursan_view->run();

// Setup login status
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$tbl_masterjursan_view->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if (!$tbl_masterjursan->isExport()) { ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "view";
var ftbl_masterjursanview = currentForm = new ew.Form("ftbl_masterjursanview", "view");

// Form_CustomValidate event
ftbl_masterjursanview.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
ftbl_masterjursanview.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
// Form object for search

</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if (!$tbl_masterjursan->isExport()) { ?>
<div class="btn-toolbar ew-toolbar">
<?php $tbl_masterjursan_view->ExportOptions->render("body") ?>
<?php $tbl_masterjursan_view->OtherOptions->render("body") ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $tbl_masterjursan_view->showPageHeader(); ?>
<?php
$tbl_masterjursan_view->showMessage();
?>
<form name="ftbl_masterjursanview" id="ftbl_masterjursanview" class="form-inline ew-form ew-view-form" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($tbl_masterjursan_view->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $tbl_masterjursan_view->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="tbl_masterjursan">
<input type="hidden" name="modal" value="<?php echo (int)$tbl_masterjursan_view->IsModal ?>">
<table class="table table-striped table-sm ew-view-table">
<?php if ($tbl_masterjursan->id_jurusan->Visible) { // id_jurusan ?>
	<tr id="r_id_jurusan">
		<td class="<?php echo $tbl_masterjursan_view->TableLeftColumnClass ?>"><span id="elh_tbl_masterjursan_id_jurusan"><?php echo $tbl_masterjursan->id_jurusan->caption() ?></span></td>
		<td data-name="id_jurusan"<?php echo $tbl_masterjursan->id_jurusan->cellAttributes() ?>>
<span id="el_tbl_masterjursan_id_jurusan">
<span<?php echo $tbl_masterjursan->id_jurusan->viewAttributes() ?>>
<?php echo $tbl_masterjursan->id_jurusan->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($tbl_masterjursan->Jurusan->Visible) { // Jurusan ?>
	<tr id="r_Jurusan">
		<td class="<?php echo $tbl_masterjursan_view->TableLeftColumnClass ?>"><span id="elh_tbl_masterjursan_Jurusan"><?php echo $tbl_masterjursan->Jurusan->caption() ?></span></td>
		<td data-name="Jurusan"<?php echo $tbl_masterjursan->Jurusan->cellAttributes() ?>>
<span id="el_tbl_masterjursan_Jurusan">
<span<?php echo $tbl_masterjursan->Jurusan->viewAttributes() ?>>
<?php echo $tbl_masterjursan->Jurusan->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
</form>
<?php
$tbl_masterjursan_view->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<?php if (!$tbl_masterjursan->isExport()) { ?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$tbl_masterjursan_view->terminate();
?>