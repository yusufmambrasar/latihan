<?php
namespace PHPMaker2019\PPDBSMK2019;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$tbl_smkn2jur_view = new tbl_smkn2jur_view();

// Run the page
$tbl_smkn2jur_view->run();

// Setup login status
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$tbl_smkn2jur_view->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if (!$tbl_smkn2jur->isExport()) { ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "view";
var ftbl_smkn2jurview = currentForm = new ew.Form("ftbl_smkn2jurview", "view");

// Form_CustomValidate event
ftbl_smkn2jurview.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
ftbl_smkn2jurview.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
ftbl_smkn2jurview.lists["x_Jurusan"] = <?php echo $tbl_smkn2jur_view->Jurusan->Lookup->toClientList() ?>;
ftbl_smkn2jurview.lists["x_Jurusan"].options = <?php echo JsonEncode($tbl_smkn2jur_view->Jurusan->lookupOptions()) ?>;

// Form object for search
</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if (!$tbl_smkn2jur->isExport()) { ?>
<div class="btn-toolbar ew-toolbar">
<?php $tbl_smkn2jur_view->ExportOptions->render("body") ?>
<?php $tbl_smkn2jur_view->OtherOptions->render("body") ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $tbl_smkn2jur_view->showPageHeader(); ?>
<?php
$tbl_smkn2jur_view->showMessage();
?>
<form name="ftbl_smkn2jurview" id="ftbl_smkn2jurview" class="form-inline ew-form ew-view-form" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($tbl_smkn2jur_view->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $tbl_smkn2jur_view->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="tbl_smkn2jur">
<input type="hidden" name="modal" value="<?php echo (int)$tbl_smkn2jur_view->IsModal ?>">
<table class="table table-striped table-sm ew-view-table">
<?php if ($tbl_smkn2jur->id_smkn2jur->Visible) { // id_smkn2jur ?>
	<tr id="r_id_smkn2jur">
		<td class="<?php echo $tbl_smkn2jur_view->TableLeftColumnClass ?>"><span id="elh_tbl_smkn2jur_id_smkn2jur"><?php echo $tbl_smkn2jur->id_smkn2jur->caption() ?></span></td>
		<td data-name="id_smkn2jur"<?php echo $tbl_smkn2jur->id_smkn2jur->cellAttributes() ?>>
<span id="el_tbl_smkn2jur_id_smkn2jur">
<span<?php echo $tbl_smkn2jur->id_smkn2jur->viewAttributes() ?>>
<?php echo $tbl_smkn2jur->id_smkn2jur->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($tbl_smkn2jur->Jurusan->Visible) { // Jurusan ?>
	<tr id="r_Jurusan">
		<td class="<?php echo $tbl_smkn2jur_view->TableLeftColumnClass ?>"><span id="elh_tbl_smkn2jur_Jurusan"><?php echo $tbl_smkn2jur->Jurusan->caption() ?></span></td>
		<td data-name="Jurusan"<?php echo $tbl_smkn2jur->Jurusan->cellAttributes() ?>>
<span id="el_tbl_smkn2jur_Jurusan">
<span<?php echo $tbl_smkn2jur->Jurusan->viewAttributes() ?>>
<?php echo $tbl_smkn2jur->Jurusan->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
</form>
<?php
$tbl_smkn2jur_view->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<?php if (!$tbl_smkn2jur->isExport()) { ?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$tbl_smkn2jur_view->terminate();
?>