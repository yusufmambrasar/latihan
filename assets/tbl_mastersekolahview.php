<?php
namespace PHPMaker2019\PPDBSMK2019;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$tbl_mastersekolah_view = new tbl_mastersekolah_view();

// Run the page
$tbl_mastersekolah_view->run();

// Setup login status
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$tbl_mastersekolah_view->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if (!$tbl_mastersekolah->isExport()) { ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "view";
var ftbl_mastersekolahview = currentForm = new ew.Form("ftbl_mastersekolahview", "view");

// Form_CustomValidate event
ftbl_mastersekolahview.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
ftbl_mastersekolahview.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
// Form object for search

</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if (!$tbl_mastersekolah->isExport()) { ?>
<div class="btn-toolbar ew-toolbar">
<?php $tbl_mastersekolah_view->ExportOptions->render("body") ?>
<?php $tbl_mastersekolah_view->OtherOptions->render("body") ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $tbl_mastersekolah_view->showPageHeader(); ?>
<?php
$tbl_mastersekolah_view->showMessage();
?>
<form name="ftbl_mastersekolahview" id="ftbl_mastersekolahview" class="form-inline ew-form ew-view-form" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($tbl_mastersekolah_view->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $tbl_mastersekolah_view->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="tbl_mastersekolah">
<input type="hidden" name="modal" value="<?php echo (int)$tbl_mastersekolah_view->IsModal ?>">
<table class="table table-striped table-sm ew-view-table">
<?php if ($tbl_mastersekolah->id_sekolah->Visible) { // id_sekolah ?>
	<tr id="r_id_sekolah">
		<td class="<?php echo $tbl_mastersekolah_view->TableLeftColumnClass ?>"><span id="elh_tbl_mastersekolah_id_sekolah"><?php echo $tbl_mastersekolah->id_sekolah->caption() ?></span></td>
		<td data-name="id_sekolah"<?php echo $tbl_mastersekolah->id_sekolah->cellAttributes() ?>>
<span id="el_tbl_mastersekolah_id_sekolah">
<span<?php echo $tbl_mastersekolah->id_sekolah->viewAttributes() ?>>
<?php echo $tbl_mastersekolah->id_sekolah->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($tbl_mastersekolah->Sekolah->Visible) { // Sekolah ?>
	<tr id="r_Sekolah">
		<td class="<?php echo $tbl_mastersekolah_view->TableLeftColumnClass ?>"><span id="elh_tbl_mastersekolah_Sekolah"><?php echo $tbl_mastersekolah->Sekolah->caption() ?></span></td>
		<td data-name="Sekolah"<?php echo $tbl_mastersekolah->Sekolah->cellAttributes() ?>>
<span id="el_tbl_mastersekolah_Sekolah">
<span<?php echo $tbl_mastersekolah->Sekolah->viewAttributes() ?>>
<?php echo $tbl_mastersekolah->Sekolah->getViewValue() ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
</form>
<?php
$tbl_mastersekolah_view->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<?php if (!$tbl_mastersekolah->isExport()) { ?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$tbl_mastersekolah_view->terminate();
?>