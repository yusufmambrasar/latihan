<?php
namespace PHPMaker2019\PPDBSMK2019;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$tbl_smkn2_edit = new tbl_smkn2_edit();

// Run the page
$tbl_smkn2_edit->run();

// Setup login status
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$tbl_smkn2_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "edit";
var ftbl_smkn2edit = currentForm = new ew.Form("ftbl_smkn2edit", "edit");

// Validate form
ftbl_smkn2edit.validate = function() {
	if (!this.validateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.getForm(), $fobj = $(fobj);
	if ($fobj.find("#confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.formKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = ["insert", "gridinsert"].includes($fobj.find("#action").val()) && $k[0];
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		<?php if ($tbl_smkn2_edit->id_sekolah->Required) { ?>
			elm = this.getElements("x" + infix + "_id_sekolah");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $tbl_smkn2->id_sekolah->caption(), $tbl_smkn2->id_sekolah->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($tbl_smkn2_edit->Sekolah->Required) { ?>
			elm = this.getElements("x" + infix + "_Sekolah");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $tbl_smkn2->Sekolah->caption(), $tbl_smkn2->Sekolah->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($tbl_smkn2_edit->id_smkn2jur->Required) { ?>
			elm = this.getElements("x" + infix + "_id_smkn2jur");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $tbl_smkn2->id_smkn2jur->caption(), $tbl_smkn2->id_smkn2jur->RequiredErrorMessage)) ?>");
		<?php } ?>

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ew.forms[val])
			if (!ew.forms[val].validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
ftbl_smkn2edit.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
ftbl_smkn2edit.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
ftbl_smkn2edit.lists["x_Sekolah"] = <?php echo $tbl_smkn2_edit->Sekolah->Lookup->toClientList() ?>;
ftbl_smkn2edit.lists["x_Sekolah"].options = <?php echo JsonEncode($tbl_smkn2_edit->Sekolah->lookupOptions()) ?>;
ftbl_smkn2edit.lists["x_id_smkn2jur"] = <?php echo $tbl_smkn2_edit->id_smkn2jur->Lookup->toClientList() ?>;
ftbl_smkn2edit.lists["x_id_smkn2jur"].options = <?php echo JsonEncode($tbl_smkn2_edit->id_smkn2jur->lookupOptions()) ?>;

// Form object for search
</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php $tbl_smkn2_edit->showPageHeader(); ?>
<?php
$tbl_smkn2_edit->showMessage();
?>
<form name="ftbl_smkn2edit" id="ftbl_smkn2edit" class="<?php echo $tbl_smkn2_edit->FormClassName ?>" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($tbl_smkn2_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $tbl_smkn2_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="tbl_smkn2">
<input type="hidden" name="action" id="action" value="update">
<input type="hidden" name="modal" value="<?php echo (int)$tbl_smkn2_edit->IsModal ?>">
<div class="ew-edit-div"><!-- page* -->
<?php if ($tbl_smkn2->id_sekolah->Visible) { // id_sekolah ?>
	<div id="r_id_sekolah" class="form-group row">
		<label id="elh_tbl_smkn2_id_sekolah" class="<?php echo $tbl_smkn2_edit->LeftColumnClass ?>"><?php echo $tbl_smkn2->id_sekolah->caption() ?><?php echo ($tbl_smkn2->id_sekolah->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $tbl_smkn2_edit->RightColumnClass ?>"><div<?php echo $tbl_smkn2->id_sekolah->cellAttributes() ?>>
<span id="el_tbl_smkn2_id_sekolah">
<span<?php echo $tbl_smkn2->id_sekolah->viewAttributes() ?>>
<input type="text" readonly class="form-control-plaintext" value="<?php echo RemoveHtml($tbl_smkn2->id_sekolah->EditValue) ?>"></span>
</span>
<input type="hidden" data-table="tbl_smkn2" data-field="x_id_sekolah" name="x_id_sekolah" id="x_id_sekolah" value="<?php echo HtmlEncode($tbl_smkn2->id_sekolah->CurrentValue) ?>">
<?php echo $tbl_smkn2->id_sekolah->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($tbl_smkn2->Sekolah->Visible) { // Sekolah ?>
	<div id="r_Sekolah" class="form-group row">
		<label id="elh_tbl_smkn2_Sekolah" for="x_Sekolah" class="<?php echo $tbl_smkn2_edit->LeftColumnClass ?>"><?php echo $tbl_smkn2->Sekolah->caption() ?><?php echo ($tbl_smkn2->Sekolah->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $tbl_smkn2_edit->RightColumnClass ?>"><div<?php echo $tbl_smkn2->Sekolah->cellAttributes() ?>>
<span id="el_tbl_smkn2_Sekolah">
<div class="btn-group ew-dropdown-list" role="group">
	<div class="btn-group" role="group">
		<button type="button" class="btn form-control dropdown-toggle ew-dropdown-toggle" aria-haspopup="true" aria-expanded="false"<?php if ($tbl_smkn2->Sekolah->ReadOnly) { ?> readonly<?php } else { ?>data-toggle="dropdown"<?php } ?>><?php echo $tbl_smkn2->Sekolah->ViewValue ?></button>
		<div id="dsl_x_Sekolah" data-repeatcolumn="1" class="dropdown-menu">
			<div class="ew-items" style="overflow-x: hidden;">
<?php echo $tbl_smkn2->Sekolah->radioButtonListHtml(TRUE, "x_Sekolah") ?>
			</div><!-- /.ew-items ##-->
		</div><!-- /.dropdown-menu ##-->
		<div id="tp_x_Sekolah" class="ew-template"><input type="radio" class="form-check-input" data-table="tbl_smkn2" data-field="x_Sekolah" data-value-separator="<?php echo $tbl_smkn2->Sekolah->displayValueSeparatorAttribute() ?>" name="x_Sekolah" id="x_Sekolah" value="{value}"<?php echo $tbl_smkn2->Sekolah->editAttributes() ?>></div>
	</div><!-- /.btn-group ##-->
	<?php if (!$tbl_smkn2->Sekolah->ReadOnly) { ?>
	<button type="button" class="btn btn-default ew-dropdown-clear" disabled>
		<i class="fa fa-times ew-icon"></i>
	</button>
<?php echo $tbl_smkn2->Sekolah->Lookup->getParamTag("p_x_Sekolah") ?>
	<?php } ?>
</div><!-- /.ew-dropdown-list ##-->
</span>
<?php echo $tbl_smkn2->Sekolah->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($tbl_smkn2->id_smkn2jur->Visible) { // id_smkn2jur ?>
	<div id="r_id_smkn2jur" class="form-group row">
		<label id="elh_tbl_smkn2_id_smkn2jur" for="x_id_smkn2jur" class="<?php echo $tbl_smkn2_edit->LeftColumnClass ?>"><?php echo $tbl_smkn2->id_smkn2jur->caption() ?><?php echo ($tbl_smkn2->id_smkn2jur->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $tbl_smkn2_edit->RightColumnClass ?>"><div<?php echo $tbl_smkn2->id_smkn2jur->cellAttributes() ?>>
<span id="el_tbl_smkn2_id_smkn2jur">
<div class="btn-group ew-dropdown-list" role="group">
	<div class="btn-group" role="group">
		<button type="button" class="btn form-control dropdown-toggle ew-dropdown-toggle" aria-haspopup="true" aria-expanded="false"<?php if ($tbl_smkn2->id_smkn2jur->ReadOnly) { ?> readonly<?php } else { ?>data-toggle="dropdown"<?php } ?>><?php echo $tbl_smkn2->id_smkn2jur->ViewValue ?></button>
		<div id="dsl_x_id_smkn2jur" data-repeatcolumn="1" class="dropdown-menu">
			<div class="ew-items" style="overflow-x: hidden;">
<?php echo $tbl_smkn2->id_smkn2jur->radioButtonListHtml(TRUE, "x_id_smkn2jur") ?>
			</div><!-- /.ew-items ##-->
		</div><!-- /.dropdown-menu ##-->
		<div id="tp_x_id_smkn2jur" class="ew-template"><input type="radio" class="form-check-input" data-table="tbl_smkn2" data-field="x_id_smkn2jur" data-value-separator="<?php echo $tbl_smkn2->id_smkn2jur->displayValueSeparatorAttribute() ?>" name="x_id_smkn2jur" id="x_id_smkn2jur" value="{value}"<?php echo $tbl_smkn2->id_smkn2jur->editAttributes() ?>></div>
	</div><!-- /.btn-group ##-->
	<?php if (!$tbl_smkn2->id_smkn2jur->ReadOnly) { ?>
	<button type="button" class="btn btn-default ew-dropdown-clear" disabled>
		<i class="fa fa-times ew-icon"></i>
	</button>
<?php echo $tbl_smkn2->id_smkn2jur->Lookup->getParamTag("p_x_id_smkn2jur") ?>
	<?php } ?>
</div><!-- /.ew-dropdown-list ##-->
</span>
<?php echo $tbl_smkn2->id_smkn2jur->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div><!-- /page* -->
<?php if (!$tbl_smkn2_edit->IsModal) { ?>
<div class="form-group row"><!-- buttons .form-group -->
	<div class="<?php echo $tbl_smkn2_edit->OffsetColumnClass ?>"><!-- buttons offset -->
<button class="btn btn-primary ew-btn" name="btn-action" id="btn-action" type="submit"><?php echo $Language->phrase("SaveBtn") ?></button>
<button class="btn btn-default ew-btn" name="btn-cancel" id="btn-cancel" type="button" data-href="<?php echo $tbl_smkn2_edit->getReturnUrl() ?>"><?php echo $Language->phrase("CancelBtn") ?></button>
	</div><!-- /buttons offset -->
</div><!-- /buttons .form-group -->
<?php } ?>
</form>
<?php
$tbl_smkn2_edit->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$tbl_smkn2_edit->terminate();
?>