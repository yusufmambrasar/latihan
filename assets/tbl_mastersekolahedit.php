<?php
namespace PHPMaker2019\PPDBSMK2019;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$tbl_mastersekolah_edit = new tbl_mastersekolah_edit();

// Run the page
$tbl_mastersekolah_edit->run();

// Setup login status
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$tbl_mastersekolah_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "edit";
var ftbl_mastersekolahedit = currentForm = new ew.Form("ftbl_mastersekolahedit", "edit");

// Validate form
ftbl_mastersekolahedit.validate = function() {
	if (!this.validateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.getForm(), $fobj = $(fobj);
	if ($fobj.find("#confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.formKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = ["insert", "gridinsert"].includes($fobj.find("#action").val()) && $k[0];
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		<?php if ($tbl_mastersekolah_edit->id_sekolah->Required) { ?>
			elm = this.getElements("x" + infix + "_id_sekolah");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $tbl_mastersekolah->id_sekolah->caption(), $tbl_mastersekolah->id_sekolah->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($tbl_mastersekolah_edit->Sekolah->Required) { ?>
			elm = this.getElements("x" + infix + "_Sekolah");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $tbl_mastersekolah->Sekolah->caption(), $tbl_mastersekolah->Sekolah->RequiredErrorMessage)) ?>");
		<?php } ?>

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ew.forms[val])
			if (!ew.forms[val].validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
ftbl_mastersekolahedit.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
ftbl_mastersekolahedit.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
// Form object for search

</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php $tbl_mastersekolah_edit->showPageHeader(); ?>
<?php
$tbl_mastersekolah_edit->showMessage();
?>
<form name="ftbl_mastersekolahedit" id="ftbl_mastersekolahedit" class="<?php echo $tbl_mastersekolah_edit->FormClassName ?>" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($tbl_mastersekolah_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $tbl_mastersekolah_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="tbl_mastersekolah">
<input type="hidden" name="action" id="action" value="update">
<input type="hidden" name="modal" value="<?php echo (int)$tbl_mastersekolah_edit->IsModal ?>">
<div class="ew-edit-div"><!-- page* -->
<?php if ($tbl_mastersekolah->id_sekolah->Visible) { // id_sekolah ?>
	<div id="r_id_sekolah" class="form-group row">
		<label id="elh_tbl_mastersekolah_id_sekolah" class="<?php echo $tbl_mastersekolah_edit->LeftColumnClass ?>"><?php echo $tbl_mastersekolah->id_sekolah->caption() ?><?php echo ($tbl_mastersekolah->id_sekolah->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $tbl_mastersekolah_edit->RightColumnClass ?>"><div<?php echo $tbl_mastersekolah->id_sekolah->cellAttributes() ?>>
<span id="el_tbl_mastersekolah_id_sekolah">
<span<?php echo $tbl_mastersekolah->id_sekolah->viewAttributes() ?>>
<input type="text" readonly class="form-control-plaintext" value="<?php echo RemoveHtml($tbl_mastersekolah->id_sekolah->EditValue) ?>"></span>
</span>
<input type="hidden" data-table="tbl_mastersekolah" data-field="x_id_sekolah" name="x_id_sekolah" id="x_id_sekolah" value="<?php echo HtmlEncode($tbl_mastersekolah->id_sekolah->CurrentValue) ?>">
<?php echo $tbl_mastersekolah->id_sekolah->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($tbl_mastersekolah->Sekolah->Visible) { // Sekolah ?>
	<div id="r_Sekolah" class="form-group row">
		<label id="elh_tbl_mastersekolah_Sekolah" for="x_Sekolah" class="<?php echo $tbl_mastersekolah_edit->LeftColumnClass ?>"><?php echo $tbl_mastersekolah->Sekolah->caption() ?><?php echo ($tbl_mastersekolah->Sekolah->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $tbl_mastersekolah_edit->RightColumnClass ?>"><div<?php echo $tbl_mastersekolah->Sekolah->cellAttributes() ?>>
<span id="el_tbl_mastersekolah_Sekolah">
<input type="text" data-table="tbl_mastersekolah" data-field="x_Sekolah" name="x_Sekolah" id="x_Sekolah" size="30" maxlength="50" placeholder="<?php echo HtmlEncode($tbl_mastersekolah->Sekolah->getPlaceHolder()) ?>" value="<?php echo $tbl_mastersekolah->Sekolah->EditValue ?>"<?php echo $tbl_mastersekolah->Sekolah->editAttributes() ?>>
</span>
<?php echo $tbl_mastersekolah->Sekolah->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div><!-- /page* -->
<?php if (!$tbl_mastersekolah_edit->IsModal) { ?>
<div class="form-group row"><!-- buttons .form-group -->
	<div class="<?php echo $tbl_mastersekolah_edit->OffsetColumnClass ?>"><!-- buttons offset -->
<button class="btn btn-primary ew-btn" name="btn-action" id="btn-action" type="submit"><?php echo $Language->phrase("SaveBtn") ?></button>
<button class="btn btn-default ew-btn" name="btn-cancel" id="btn-cancel" type="button" data-href="<?php echo $tbl_mastersekolah_edit->getReturnUrl() ?>"><?php echo $Language->phrase("CancelBtn") ?></button>
	</div><!-- /buttons offset -->
</div><!-- /buttons .form-group -->
<?php } ?>
</form>
<?php
$tbl_mastersekolah_edit->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$tbl_mastersekolah_edit->terminate();
?>