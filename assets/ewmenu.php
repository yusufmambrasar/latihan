<?php
namespace PHPMaker2019\PPDBSMK2019;

// Menu Language
if ($Language && $Language->LanguageFolder == $LANGUAGE_FOLDER)
	$MenuLanguage = &$Language;
else
	$MenuLanguage = new Language();

// Navbar menu
$topMenu = new Menu("navbar", TRUE, TRUE);
echo $topMenu->toScript();

// Sidebar menu
$sideMenu = new Menu("menu", TRUE, FALSE);
$sideMenu->addMenuItem(1, "mi_tbl_daftar", $MenuLanguage->MenuPhrase("1", "MenuText"), "tbl_daftarlist.php", -1, "", TRUE, FALSE, FALSE, "", "", FALSE);
$sideMenu->addMenuItem(2, "mi_tbl_masterjursan", $MenuLanguage->MenuPhrase("2", "MenuText"), "tbl_masterjursanlist.php", -1, "", TRUE, FALSE, FALSE, "", "", FALSE);
$sideMenu->addMenuItem(3, "mi_tbl_mastersekolah", $MenuLanguage->MenuPhrase("3", "MenuText"), "tbl_mastersekolahlist.php", -1, "", TRUE, FALSE, FALSE, "", "", FALSE);
$sideMenu->addMenuItem(4, "mi_tbl_smkn2", $MenuLanguage->MenuPhrase("4", "MenuText"), "tbl_smkn2list.php", -1, "", TRUE, FALSE, FALSE, "", "", FALSE);
$sideMenu->addMenuItem(5, "mi_tbl_smkn2jur", $MenuLanguage->MenuPhrase("5", "MenuText"), "tbl_smkn2jurlist.php", -1, "", TRUE, FALSE, FALSE, "", "", FALSE);
$sideMenu->addMenuItem(6, "mi_tbl_smkn3", $MenuLanguage->MenuPhrase("6", "MenuText"), "tbl_smkn3list.php", -1, "", TRUE, FALSE, FALSE, "", "", FALSE);
$sideMenu->addMenuItem(7, "mi_tbl_smkn3jur", $MenuLanguage->MenuPhrase("7", "MenuText"), "tbl_smkn3jurlist.php", -1, "", TRUE, FALSE, FALSE, "", "", FALSE);
echo $sideMenu->toScript();
?>