<?php
namespace PHPMaker2019\PPDBSMK2019;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$tbl_smkn3jur_add = new tbl_smkn3jur_add();

// Run the page
$tbl_smkn3jur_add->run();

// Setup login status
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$tbl_smkn3jur_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "add";
var ftbl_smkn3juradd = currentForm = new ew.Form("ftbl_smkn3juradd", "add");

// Validate form
ftbl_smkn3juradd.validate = function() {
	if (!this.validateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.getForm(), $fobj = $(fobj);
	if ($fobj.find("#confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.formKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = ["insert", "gridinsert"].includes($fobj.find("#action").val()) && $k[0];
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		<?php if ($tbl_smkn3jur_add->Jurusan->Required) { ?>
			elm = this.getElements("x" + infix + "_Jurusan");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $tbl_smkn3jur->Jurusan->caption(), $tbl_smkn3jur->Jurusan->RequiredErrorMessage)) ?>");
		<?php } ?>

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ew.forms[val])
			if (!ew.forms[val].validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
ftbl_smkn3juradd.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
ftbl_smkn3juradd.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
// Form object for search

</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php $tbl_smkn3jur_add->showPageHeader(); ?>
<?php
$tbl_smkn3jur_add->showMessage();
?>
<form name="ftbl_smkn3juradd" id="ftbl_smkn3juradd" class="<?php echo $tbl_smkn3jur_add->FormClassName ?>" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($tbl_smkn3jur_add->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $tbl_smkn3jur_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="tbl_smkn3jur">
<input type="hidden" name="action" id="action" value="insert">
<input type="hidden" name="modal" value="<?php echo (int)$tbl_smkn3jur_add->IsModal ?>">
<div class="ew-add-div"><!-- page* -->
<?php if ($tbl_smkn3jur->Jurusan->Visible) { // Jurusan ?>
	<div id="r_Jurusan" class="form-group row">
		<label id="elh_tbl_smkn3jur_Jurusan" for="x_Jurusan" class="<?php echo $tbl_smkn3jur_add->LeftColumnClass ?>"><?php echo $tbl_smkn3jur->Jurusan->caption() ?><?php echo ($tbl_smkn3jur->Jurusan->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $tbl_smkn3jur_add->RightColumnClass ?>"><div<?php echo $tbl_smkn3jur->Jurusan->cellAttributes() ?>>
<span id="el_tbl_smkn3jur_Jurusan">
<input type="text" data-table="tbl_smkn3jur" data-field="x_Jurusan" name="x_Jurusan" id="x_Jurusan" size="30" maxlength="50" placeholder="<?php echo HtmlEncode($tbl_smkn3jur->Jurusan->getPlaceHolder()) ?>" value="<?php echo $tbl_smkn3jur->Jurusan->EditValue ?>"<?php echo $tbl_smkn3jur->Jurusan->editAttributes() ?>>
</span>
<?php echo $tbl_smkn3jur->Jurusan->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div><!-- /page* -->
<?php if (!$tbl_smkn3jur_add->IsModal) { ?>
<div class="form-group row"><!-- buttons .form-group -->
	<div class="<?php echo $tbl_smkn3jur_add->OffsetColumnClass ?>"><!-- buttons offset -->
<button class="btn btn-primary ew-btn" name="btn-action" id="btn-action" type="submit"><?php echo $Language->phrase("AddBtn") ?></button>
<button class="btn btn-default ew-btn" name="btn-cancel" id="btn-cancel" type="button" data-href="<?php echo $tbl_smkn3jur_add->getReturnUrl() ?>"><?php echo $Language->phrase("CancelBtn") ?></button>
	</div><!-- /buttons offset -->
</div><!-- /buttons .form-group -->
<?php } ?>
</form>
<?php
$tbl_smkn3jur_add->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$tbl_smkn3jur_add->terminate();
?>