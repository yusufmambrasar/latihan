<?php
namespace PHPMaker2019\PPDBSMK2019;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$tbl_smkn2_add = new tbl_smkn2_add();

// Run the page
$tbl_smkn2_add->run();

// Setup login status
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$tbl_smkn2_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "add";
var ftbl_smkn2add = currentForm = new ew.Form("ftbl_smkn2add", "add");

// Validate form
ftbl_smkn2add.validate = function() {
	if (!this.validateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.getForm(), $fobj = $(fobj);
	if ($fobj.find("#confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.formKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = ["insert", "gridinsert"].includes($fobj.find("#action").val()) && $k[0];
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		<?php if ($tbl_smkn2_add->Sekolah->Required) { ?>
			elm = this.getElements("x" + infix + "_Sekolah");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $tbl_smkn2->Sekolah->caption(), $tbl_smkn2->Sekolah->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($tbl_smkn2_add->id_smkn2jur->Required) { ?>
			elm = this.getElements("x" + infix + "_id_smkn2jur");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $tbl_smkn2->id_smkn2jur->caption(), $tbl_smkn2->id_smkn2jur->RequiredErrorMessage)) ?>");
		<?php } ?>

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ew.forms[val])
			if (!ew.forms[val].validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
ftbl_smkn2add.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
ftbl_smkn2add.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
ftbl_smkn2add.lists["x_Sekolah"] = <?php echo $tbl_smkn2_add->Sekolah->Lookup->toClientList() ?>;
ftbl_smkn2add.lists["x_Sekolah"].options = <?php echo JsonEncode($tbl_smkn2_add->Sekolah->lookupOptions()) ?>;
ftbl_smkn2add.lists["x_id_smkn2jur"] = <?php echo $tbl_smkn2_add->id_smkn2jur->Lookup->toClientList() ?>;
ftbl_smkn2add.lists["x_id_smkn2jur"].options = <?php echo JsonEncode($tbl_smkn2_add->id_smkn2jur->lookupOptions()) ?>;

// Form object for search
</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php $tbl_smkn2_add->showPageHeader(); ?>
<?php
$tbl_smkn2_add->showMessage();
?>
<form name="ftbl_smkn2add" id="ftbl_smkn2add" class="<?php echo $tbl_smkn2_add->FormClassName ?>" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($tbl_smkn2_add->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $tbl_smkn2_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="tbl_smkn2">
<input type="hidden" name="action" id="action" value="insert">
<input type="hidden" name="modal" value="<?php echo (int)$tbl_smkn2_add->IsModal ?>">
<div class="ew-add-div"><!-- page* -->
<?php if ($tbl_smkn2->Sekolah->Visible) { // Sekolah ?>
	<div id="r_Sekolah" class="form-group row">
		<label id="elh_tbl_smkn2_Sekolah" for="x_Sekolah" class="<?php echo $tbl_smkn2_add->LeftColumnClass ?>"><?php echo $tbl_smkn2->Sekolah->caption() ?><?php echo ($tbl_smkn2->Sekolah->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $tbl_smkn2_add->RightColumnClass ?>"><div<?php echo $tbl_smkn2->Sekolah->cellAttributes() ?>>
<span id="el_tbl_smkn2_Sekolah">
<div class="btn-group ew-dropdown-list" role="group">
	<div class="btn-group" role="group">
		<button type="button" class="btn form-control dropdown-toggle ew-dropdown-toggle" aria-haspopup="true" aria-expanded="false"<?php if ($tbl_smkn2->Sekolah->ReadOnly) { ?> readonly<?php } else { ?>data-toggle="dropdown"<?php } ?>><?php echo $tbl_smkn2->Sekolah->ViewValue ?></button>
		<div id="dsl_x_Sekolah" data-repeatcolumn="1" class="dropdown-menu">
			<div class="ew-items" style="overflow-x: hidden;">
<?php echo $tbl_smkn2->Sekolah->radioButtonListHtml(TRUE, "x_Sekolah") ?>
			</div><!-- /.ew-items ##-->
		</div><!-- /.dropdown-menu ##-->
		<div id="tp_x_Sekolah" class="ew-template"><input type="radio" class="form-check-input" data-table="tbl_smkn2" data-field="x_Sekolah" data-value-separator="<?php echo $tbl_smkn2->Sekolah->displayValueSeparatorAttribute() ?>" name="x_Sekolah" id="x_Sekolah" value="{value}"<?php echo $tbl_smkn2->Sekolah->editAttributes() ?>></div>
	</div><!-- /.btn-group ##-->
	<?php if (!$tbl_smkn2->Sekolah->ReadOnly) { ?>
	<button type="button" class="btn btn-default ew-dropdown-clear" disabled>
		<i class="fa fa-times ew-icon"></i>
	</button>
<?php echo $tbl_smkn2->Sekolah->Lookup->getParamTag("p_x_Sekolah") ?>
	<?php } ?>
</div><!-- /.ew-dropdown-list ##-->
</span>
<?php echo $tbl_smkn2->Sekolah->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($tbl_smkn2->id_smkn2jur->Visible) { // id_smkn2jur ?>
	<div id="r_id_smkn2jur" class="form-group row">
		<label id="elh_tbl_smkn2_id_smkn2jur" for="x_id_smkn2jur" class="<?php echo $tbl_smkn2_add->LeftColumnClass ?>"><?php echo $tbl_smkn2->id_smkn2jur->caption() ?><?php echo ($tbl_smkn2->id_smkn2jur->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $tbl_smkn2_add->RightColumnClass ?>"><div<?php echo $tbl_smkn2->id_smkn2jur->cellAttributes() ?>>
<span id="el_tbl_smkn2_id_smkn2jur">
<div class="btn-group ew-dropdown-list" role="group">
	<div class="btn-group" role="group">
		<button type="button" class="btn form-control dropdown-toggle ew-dropdown-toggle" aria-haspopup="true" aria-expanded="false"<?php if ($tbl_smkn2->id_smkn2jur->ReadOnly) { ?> readonly<?php } else { ?>data-toggle="dropdown"<?php } ?>><?php echo $tbl_smkn2->id_smkn2jur->ViewValue ?></button>
		<div id="dsl_x_id_smkn2jur" data-repeatcolumn="1" class="dropdown-menu">
			<div class="ew-items" style="overflow-x: hidden;">
<?php echo $tbl_smkn2->id_smkn2jur->radioButtonListHtml(TRUE, "x_id_smkn2jur") ?>
			</div><!-- /.ew-items ##-->
		</div><!-- /.dropdown-menu ##-->
		<div id="tp_x_id_smkn2jur" class="ew-template"><input type="radio" class="form-check-input" data-table="tbl_smkn2" data-field="x_id_smkn2jur" data-value-separator="<?php echo $tbl_smkn2->id_smkn2jur->displayValueSeparatorAttribute() ?>" name="x_id_smkn2jur" id="x_id_smkn2jur" value="{value}"<?php echo $tbl_smkn2->id_smkn2jur->editAttributes() ?>></div>
	</div><!-- /.btn-group ##-->
	<?php if (!$tbl_smkn2->id_smkn2jur->ReadOnly) { ?>
	<button type="button" class="btn btn-default ew-dropdown-clear" disabled>
		<i class="fa fa-times ew-icon"></i>
	</button>
<?php echo $tbl_smkn2->id_smkn2jur->Lookup->getParamTag("p_x_id_smkn2jur") ?>
	<?php } ?>
</div><!-- /.ew-dropdown-list ##-->
</span>
<?php echo $tbl_smkn2->id_smkn2jur->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div><!-- /page* -->
<?php if (!$tbl_smkn2_add->IsModal) { ?>
<div class="form-group row"><!-- buttons .form-group -->
	<div class="<?php echo $tbl_smkn2_add->OffsetColumnClass ?>"><!-- buttons offset -->
<button class="btn btn-primary ew-btn" name="btn-action" id="btn-action" type="submit"><?php echo $Language->phrase("AddBtn") ?></button>
<button class="btn btn-default ew-btn" name="btn-cancel" id="btn-cancel" type="button" data-href="<?php echo $tbl_smkn2_add->getReturnUrl() ?>"><?php echo $Language->phrase("CancelBtn") ?></button>
	</div><!-- /buttons offset -->
</div><!-- /buttons .form-group -->
<?php } ?>
</form>
<?php
$tbl_smkn2_add->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$tbl_smkn2_add->terminate();
?>