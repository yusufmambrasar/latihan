<?php
namespace PHPMaker2019\PPDBSMK2019;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$tbl_masterjursan_delete = new tbl_masterjursan_delete();

// Run the page
$tbl_masterjursan_delete->run();

// Setup login status
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$tbl_masterjursan_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "delete";
var ftbl_masterjursandelete = currentForm = new ew.Form("ftbl_masterjursandelete", "delete");

// Form_CustomValidate event
ftbl_masterjursandelete.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
ftbl_masterjursandelete.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
// Form object for search

</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php $tbl_masterjursan_delete->showPageHeader(); ?>
<?php
$tbl_masterjursan_delete->showMessage();
?>
<form name="ftbl_masterjursandelete" id="ftbl_masterjursandelete" class="form-inline ew-form ew-delete-form" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($tbl_masterjursan_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $tbl_masterjursan_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="tbl_masterjursan">
<input type="hidden" name="action" id="action" value="delete">
<?php foreach ($tbl_masterjursan_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="card ew-card ew-grid">
<div class="<?php if (IsResponsiveLayout()) { ?>table-responsive <?php } ?>card-body ew-grid-middle-panel">
<table class="table ew-table">
	<thead>
	<tr class="ew-table-header">
<?php if ($tbl_masterjursan->id_jurusan->Visible) { // id_jurusan ?>
		<th class="<?php echo $tbl_masterjursan->id_jurusan->headerCellClass() ?>"><span id="elh_tbl_masterjursan_id_jurusan" class="tbl_masterjursan_id_jurusan"><?php echo $tbl_masterjursan->id_jurusan->caption() ?></span></th>
<?php } ?>
<?php if ($tbl_masterjursan->Jurusan->Visible) { // Jurusan ?>
		<th class="<?php echo $tbl_masterjursan->Jurusan->headerCellClass() ?>"><span id="elh_tbl_masterjursan_Jurusan" class="tbl_masterjursan_Jurusan"><?php echo $tbl_masterjursan->Jurusan->caption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$tbl_masterjursan_delete->RecCnt = 0;
$i = 0;
while (!$tbl_masterjursan_delete->Recordset->EOF) {
	$tbl_masterjursan_delete->RecCnt++;
	$tbl_masterjursan_delete->RowCnt++;

	// Set row properties
	$tbl_masterjursan->resetAttributes();
	$tbl_masterjursan->RowType = ROWTYPE_VIEW; // View

	// Get the field contents
	$tbl_masterjursan_delete->loadRowValues($tbl_masterjursan_delete->Recordset);

	// Render row
	$tbl_masterjursan_delete->renderRow();
?>
	<tr<?php echo $tbl_masterjursan->rowAttributes() ?>>
<?php if ($tbl_masterjursan->id_jurusan->Visible) { // id_jurusan ?>
		<td<?php echo $tbl_masterjursan->id_jurusan->cellAttributes() ?>>
<span id="el<?php echo $tbl_masterjursan_delete->RowCnt ?>_tbl_masterjursan_id_jurusan" class="tbl_masterjursan_id_jurusan">
<span<?php echo $tbl_masterjursan->id_jurusan->viewAttributes() ?>>
<?php echo $tbl_masterjursan->id_jurusan->getViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($tbl_masterjursan->Jurusan->Visible) { // Jurusan ?>
		<td<?php echo $tbl_masterjursan->Jurusan->cellAttributes() ?>>
<span id="el<?php echo $tbl_masterjursan_delete->RowCnt ?>_tbl_masterjursan_Jurusan" class="tbl_masterjursan_Jurusan">
<span<?php echo $tbl_masterjursan->Jurusan->viewAttributes() ?>>
<?php echo $tbl_masterjursan->Jurusan->getViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$tbl_masterjursan_delete->Recordset->moveNext();
}
$tbl_masterjursan_delete->Recordset->close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ew-btn" name="btn-action" id="btn-action" type="submit"><?php echo $Language->phrase("DeleteBtn") ?></button>
<button class="btn btn-default ew-btn" name="btn-cancel" id="btn-cancel" type="button" data-href="<?php echo $tbl_masterjursan_delete->getReturnUrl() ?>"><?php echo $Language->phrase("CancelBtn") ?></button>
</div>
</form>
<?php
$tbl_masterjursan_delete->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$tbl_masterjursan_delete->terminate();
?>