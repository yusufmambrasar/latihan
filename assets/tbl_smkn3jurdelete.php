<?php
namespace PHPMaker2019\PPDBSMK2019;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$tbl_smkn3jur_delete = new tbl_smkn3jur_delete();

// Run the page
$tbl_smkn3jur_delete->run();

// Setup login status
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$tbl_smkn3jur_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "delete";
var ftbl_smkn3jurdelete = currentForm = new ew.Form("ftbl_smkn3jurdelete", "delete");

// Form_CustomValidate event
ftbl_smkn3jurdelete.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
ftbl_smkn3jurdelete.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
// Form object for search

</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php $tbl_smkn3jur_delete->showPageHeader(); ?>
<?php
$tbl_smkn3jur_delete->showMessage();
?>
<form name="ftbl_smkn3jurdelete" id="ftbl_smkn3jurdelete" class="form-inline ew-form ew-delete-form" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($tbl_smkn3jur_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $tbl_smkn3jur_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="tbl_smkn3jur">
<input type="hidden" name="action" id="action" value="delete">
<?php foreach ($tbl_smkn3jur_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="card ew-card ew-grid">
<div class="<?php if (IsResponsiveLayout()) { ?>table-responsive <?php } ?>card-body ew-grid-middle-panel">
<table class="table ew-table">
	<thead>
	<tr class="ew-table-header">
<?php if ($tbl_smkn3jur->id_smkn2jur->Visible) { // id_smkn2jur ?>
		<th class="<?php echo $tbl_smkn3jur->id_smkn2jur->headerCellClass() ?>"><span id="elh_tbl_smkn3jur_id_smkn2jur" class="tbl_smkn3jur_id_smkn2jur"><?php echo $tbl_smkn3jur->id_smkn2jur->caption() ?></span></th>
<?php } ?>
<?php if ($tbl_smkn3jur->Jurusan->Visible) { // Jurusan ?>
		<th class="<?php echo $tbl_smkn3jur->Jurusan->headerCellClass() ?>"><span id="elh_tbl_smkn3jur_Jurusan" class="tbl_smkn3jur_Jurusan"><?php echo $tbl_smkn3jur->Jurusan->caption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$tbl_smkn3jur_delete->RecCnt = 0;
$i = 0;
while (!$tbl_smkn3jur_delete->Recordset->EOF) {
	$tbl_smkn3jur_delete->RecCnt++;
	$tbl_smkn3jur_delete->RowCnt++;

	// Set row properties
	$tbl_smkn3jur->resetAttributes();
	$tbl_smkn3jur->RowType = ROWTYPE_VIEW; // View

	// Get the field contents
	$tbl_smkn3jur_delete->loadRowValues($tbl_smkn3jur_delete->Recordset);

	// Render row
	$tbl_smkn3jur_delete->renderRow();
?>
	<tr<?php echo $tbl_smkn3jur->rowAttributes() ?>>
<?php if ($tbl_smkn3jur->id_smkn2jur->Visible) { // id_smkn2jur ?>
		<td<?php echo $tbl_smkn3jur->id_smkn2jur->cellAttributes() ?>>
<span id="el<?php echo $tbl_smkn3jur_delete->RowCnt ?>_tbl_smkn3jur_id_smkn2jur" class="tbl_smkn3jur_id_smkn2jur">
<span<?php echo $tbl_smkn3jur->id_smkn2jur->viewAttributes() ?>>
<?php echo $tbl_smkn3jur->id_smkn2jur->getViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($tbl_smkn3jur->Jurusan->Visible) { // Jurusan ?>
		<td<?php echo $tbl_smkn3jur->Jurusan->cellAttributes() ?>>
<span id="el<?php echo $tbl_smkn3jur_delete->RowCnt ?>_tbl_smkn3jur_Jurusan" class="tbl_smkn3jur_Jurusan">
<span<?php echo $tbl_smkn3jur->Jurusan->viewAttributes() ?>>
<?php echo $tbl_smkn3jur->Jurusan->getViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$tbl_smkn3jur_delete->Recordset->moveNext();
}
$tbl_smkn3jur_delete->Recordset->close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ew-btn" name="btn-action" id="btn-action" type="submit"><?php echo $Language->phrase("DeleteBtn") ?></button>
<button class="btn btn-default ew-btn" name="btn-cancel" id="btn-cancel" type="button" data-href="<?php echo $tbl_smkn3jur_delete->getReturnUrl() ?>"><?php echo $Language->phrase("CancelBtn") ?></button>
</div>
</form>
<?php
$tbl_smkn3jur_delete->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$tbl_smkn3jur_delete->terminate();
?>