<?php
namespace PHPMaker2019\PPDBSMK2019;

// Session
if (session_status() !== PHP_SESSION_ACTIVE)
	session_start(); // Init session data

// Output buffering
ob_start(); 

// Autoload
include_once "autoload.php";
?>
<?php

// Write header
WriteHeader(FALSE);

// Create page object
$tbl_masterjursan_edit = new tbl_masterjursan_edit();

// Run the page
$tbl_masterjursan_edit->run();

// Setup login status
SetClientVar("login", LoginStatus());

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$tbl_masterjursan_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script>

// Form object
currentPageID = ew.PAGE_ID = "edit";
var ftbl_masterjursanedit = currentForm = new ew.Form("ftbl_masterjursanedit", "edit");

// Validate form
ftbl_masterjursanedit.validate = function() {
	if (!this.validateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.getForm(), $fobj = $(fobj);
	if ($fobj.find("#confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.formKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = ["insert", "gridinsert"].includes($fobj.find("#action").val()) && $k[0];
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		<?php if ($tbl_masterjursan_edit->id_jurusan->Required) { ?>
			elm = this.getElements("x" + infix + "_id_jurusan");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $tbl_masterjursan->id_jurusan->caption(), $tbl_masterjursan->id_jurusan->RequiredErrorMessage)) ?>");
		<?php } ?>
		<?php if ($tbl_masterjursan_edit->Jurusan->Required) { ?>
			elm = this.getElements("x" + infix + "_Jurusan");
			if (elm && !ew.isHidden(elm) && !ew.hasValue(elm))
				return this.onError(elm, "<?php echo JsEncode(str_replace("%s", $tbl_masterjursan->Jurusan->caption(), $tbl_masterjursan->Jurusan->RequiredErrorMessage)) ?>");
		<?php } ?>

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ew.forms[val])
			if (!ew.forms[val].validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
ftbl_masterjursanedit.Form_CustomValidate = function(fobj) { // DO NOT CHANGE THIS LINE!

	// Your custom validation code here, return false if invalid.
	return true;
}

// Use JavaScript validation or not
ftbl_masterjursanedit.validateRequired = <?php echo json_encode(CLIENT_VALIDATE) ?>;

// Dynamic selection lists
// Form object for search

</script>
<script>

// Write your client script here, no need to add script tags.
</script>
<?php $tbl_masterjursan_edit->showPageHeader(); ?>
<?php
$tbl_masterjursan_edit->showMessage();
?>
<form name="ftbl_masterjursanedit" id="ftbl_masterjursanedit" class="<?php echo $tbl_masterjursan_edit->FormClassName ?>" action="<?php echo CurrentPageName() ?>" method="post">
<?php if ($tbl_masterjursan_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo TOKEN_NAME ?>" value="<?php echo $tbl_masterjursan_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="tbl_masterjursan">
<input type="hidden" name="action" id="action" value="update">
<input type="hidden" name="modal" value="<?php echo (int)$tbl_masterjursan_edit->IsModal ?>">
<div class="ew-edit-div"><!-- page* -->
<?php if ($tbl_masterjursan->id_jurusan->Visible) { // id_jurusan ?>
	<div id="r_id_jurusan" class="form-group row">
		<label id="elh_tbl_masterjursan_id_jurusan" class="<?php echo $tbl_masterjursan_edit->LeftColumnClass ?>"><?php echo $tbl_masterjursan->id_jurusan->caption() ?><?php echo ($tbl_masterjursan->id_jurusan->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $tbl_masterjursan_edit->RightColumnClass ?>"><div<?php echo $tbl_masterjursan->id_jurusan->cellAttributes() ?>>
<span id="el_tbl_masterjursan_id_jurusan">
<span<?php echo $tbl_masterjursan->id_jurusan->viewAttributes() ?>>
<input type="text" readonly class="form-control-plaintext" value="<?php echo RemoveHtml($tbl_masterjursan->id_jurusan->EditValue) ?>"></span>
</span>
<input type="hidden" data-table="tbl_masterjursan" data-field="x_id_jurusan" name="x_id_jurusan" id="x_id_jurusan" value="<?php echo HtmlEncode($tbl_masterjursan->id_jurusan->CurrentValue) ?>">
<?php echo $tbl_masterjursan->id_jurusan->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($tbl_masterjursan->Jurusan->Visible) { // Jurusan ?>
	<div id="r_Jurusan" class="form-group row">
		<label id="elh_tbl_masterjursan_Jurusan" for="x_Jurusan" class="<?php echo $tbl_masterjursan_edit->LeftColumnClass ?>"><?php echo $tbl_masterjursan->Jurusan->caption() ?><?php echo ($tbl_masterjursan->Jurusan->Required) ? $Language->phrase("FieldRequiredIndicator") : "" ?></label>
		<div class="<?php echo $tbl_masterjursan_edit->RightColumnClass ?>"><div<?php echo $tbl_masterjursan->Jurusan->cellAttributes() ?>>
<span id="el_tbl_masterjursan_Jurusan">
<input type="text" data-table="tbl_masterjursan" data-field="x_Jurusan" name="x_Jurusan" id="x_Jurusan" size="30" maxlength="50" placeholder="<?php echo HtmlEncode($tbl_masterjursan->Jurusan->getPlaceHolder()) ?>" value="<?php echo $tbl_masterjursan->Jurusan->EditValue ?>"<?php echo $tbl_masterjursan->Jurusan->editAttributes() ?>>
</span>
<?php echo $tbl_masterjursan->Jurusan->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div><!-- /page* -->
<?php if (!$tbl_masterjursan_edit->IsModal) { ?>
<div class="form-group row"><!-- buttons .form-group -->
	<div class="<?php echo $tbl_masterjursan_edit->OffsetColumnClass ?>"><!-- buttons offset -->
<button class="btn btn-primary ew-btn" name="btn-action" id="btn-action" type="submit"><?php echo $Language->phrase("SaveBtn") ?></button>
<button class="btn btn-default ew-btn" name="btn-cancel" id="btn-cancel" type="button" data-href="<?php echo $tbl_masterjursan_edit->getReturnUrl() ?>"><?php echo $Language->phrase("CancelBtn") ?></button>
	</div><!-- /buttons offset -->
</div><!-- /buttons .form-group -->
<?php } ?>
</form>
<?php
$tbl_masterjursan_edit->showPageFooter();
if (DEBUG_ENABLED)
	echo GetDebugMessage();
?>
<script>

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$tbl_masterjursan_edit->terminate();
?>